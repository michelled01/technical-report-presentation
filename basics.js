// @ts-nocheck
// There are 3 base types in TypeScript:
var subjectArea = "CS";
var courseCode = 373;
var active = true;
// If initializing from literals, the compiler can infer the types,
// so type annotation is not required.
var subjectArea2 = "CS";
var courseCode2 = 373;
var active2 = true;
// typing arrays
var list1 = [1, 2, 3];
var list2 = [1, 2, 3];
var list3 = [1, 2, 3];
// If type annotations are omitted in contexts where the compiler cannot infer the types, the
// behavior depends on the compiler flag noImplicitAny. If enabled,
// the compiler will throw an error; otherwise, the compiler will implicitly assign the any type.
// The any type can also be explicitly given in a type annotation:
var response = {
    data: {
        id: 2,
        description: "A description",
        // let's assume the API returns an unknown type for the property below
        mysteryType: "Who knows what this could be?",
    },
};
var data = response.data; // What's the problem with this?
// tuples in TS
var tuple = ["e1", 1, {}];
tuple = ["e2", 2]; //-> Type '[string, number]' is not assignable to type '[string, number, any]'.
// tuples don't exist in JS
tuple.push(false); // will this work at runtime?
//////////////////////////////////
// Typing functions
/////////////////////////////////
function display(message) {
    console.log("Hello world");
    return 42;
}
display(4); // Argument of type 'number' is not assignable to parameter of type 'string'.
// Why don't we need typing in this case?
var message = ["Hello", "world"];
message.forEach(function (str) {
    console.log(str);
});
function displayOrNumber(message) {
    // @ts-ignore
    console.log(message.toLowerCase()); // Property 'toLowerCase' does not exist on type 'number'
    if (typeof message === "string") {
        console.log(message.toLowerCase());
    }
    else {
        console.log(message + 30);
    }
}
displayOrNumber("HELLO WORLD"); // hello world
displayOrNumber(12); // 42
