// @ts-nocheck

// creating custom types
type Point = {
  x: number;
  y: number;
};

type messageType = string | number;

// using an interface to name an object
interface ReportType {
  title: string;
  hasAuthor: boolean;
}

// Typescript is only concerned with the structure and expected behaviors of the
// object passed in. This makes TypeScript a structurally typed system.
const report: ReportType = {
  title: "The Python Library Reference",
  hasAuthor: false,
};

//////////////////////////////////
// Interfaces vs Types
/////////////////////////////////

// Modifying an interface
interface ReportType {
  published: boolean;
}

// Modifying a type
type Point = {
  // -> Error: Duplicate identifier 'Point'.
  x: number;
  y: number;
};

// Extending an interface
interface ScientificReportType extends ReportType {
  peerReviewed: boolean;
}

// Extending a type
type Point3D = Point & {
  z: number;
};

// Note, optional properties
interface AmbiguousPoint extends Point {
  z?: number
}

// For best practices, use interface until specific features from type
// are required. Types are best for smaller, primitive alises,
// while interfaces are more powerful.

//////////////////////////////////
// Example of using a Custom Type
/////////////////////////////////

const response = {
  data: {
    id: 2,
    description: "A description",
    // let's assume the API returns an unknown type for the property below
    mysteryType: "Who knows what this could be?",
  },
};

const data: any = response.data; // What's the problem with this?

// A better way
interface ResponseData {
  data: {
    id: number;
    description: string;
    mysteryType: any;
  };
}

const betterData: ResponseData = response;
