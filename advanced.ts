// @ts-nocheck

// types can be literal values, or unions of literals
const exactString: "exact" = "exact";

// Type '"Java"' is not assignable to type '"JavaScript" | "TypeScript"'
const getLanguage = (): "JavaScript" | "TypeScript" => "Java";

//////////////////////////////////
// Enums
/////////////////////////////////

// However, in most cases it's best to use an enum.
enum Author {
  Isaac,
  Jonathan,
  Michelle,
  Timothy,
  Patrick,
}
const author: Author = Author.Isaac;
console.log(Author[author]); // Isaac
console.log(author); // 0
console.log(Author["Isaac"]); // 0

enum AuthorCustom {
  Isaac,
  Jonathan = 2,
  Michelle = 8,
  Timothy,
  Patrick = 8,
}
console.log(AuthorCustom);
/* {
    "0": "Isaac",
    "2": "Jonathan",
    "8": "Patrick",
    "9": "Timothy",
    "Isaac": 0,
    "Jonathan": 2,
    "Michelle": 8,
    "Timothy": 9,
    "Patrick": 8
} */

//////////////////////////////////
// Generics
/////////////////////////////////

interface Paginated {
  page: number;
  items_per_page: number;
  total_items: number;
  items_this_page: number;
  sort_by: string;
  sort_ascending: boolean;
  items: any[]; // what's the problem with using any here?
}

interface PaginatedBetter extends Paginated {
  items: (Bill | Congressperson | Committee)[];
}

interface PaginatedBest<T> extends Paginated {
  items: T[];
}

function getItems<T>(paginatedData : PaginatedBest<T>): T[] {
    return paginatedData.items
}

const apiData : PaginatedBest<Bill> = {/* some paginated data */}
const items = getItems<Bill>(apiData)
const items2 = getItems(apiData) // typescript is able to infer the generic type in this case


export { PaginatedBest, getItems }
