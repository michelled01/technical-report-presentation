// @ts-nocheck
// Typescript is only concerned with the structure and expected behaviors of the
// object passed in. This makes TypeScript a structurally typed system.
var report = {
    title: "The Python Library Reference",
    hasAuthor: false,
};
// For best practices, use interface until specific features from type
// are required. Types are best for smaller, primitive alises,
// while interfaces are more powerful.
//////////////////////////////////
// Example of using a Custom Type
/////////////////////////////////
var response = {
    data: {
        id: 2,
        description: "A description",
        // let's assume the API returns an unknown type for the property below
        mysteryType: "Who knows what this could be?",
    },
};
var data = response.data; // What's the problem with this?
var betterData = response;
