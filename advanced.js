"use strict";
// @ts-nocheck
Object.defineProperty(exports, "__esModule", { value: true });
exports.getItems = void 0;
// types can be literal values, or unions of literals
var exactString = "exact";
// Type '"Java"' is not assignable to type '"JavaScript" | "TypeScript"'
var getLanguage = function () { return "Java"; };
//////////////////////////////////
// Enums
/////////////////////////////////
// However, in most cases it's best to use an enum.
var Author;
(function (Author) {
    Author[Author["Isaac"] = 0] = "Isaac";
    Author[Author["Jonathan"] = 1] = "Jonathan";
    Author[Author["Michelle"] = 2] = "Michelle";
    Author[Author["Timothy"] = 3] = "Timothy";
    Author[Author["Patrick"] = 4] = "Patrick";
})(Author || (Author = {}));
var author = Author.Isaac;
console.log(Author[author]); // Isaac
console.log(author); // 0
console.log(Author["Isaac"]); // 0
var AuthorCustom;
(function (AuthorCustom) {
    AuthorCustom[AuthorCustom["Isaac"] = 0] = "Isaac";
    AuthorCustom[AuthorCustom["Jonathan"] = 2] = "Jonathan";
    AuthorCustom[AuthorCustom["Michelle"] = 8] = "Michelle";
    AuthorCustom[AuthorCustom["Timothy"] = 9] = "Timothy";
    AuthorCustom[AuthorCustom["Patrick"] = 8] = "Patrick";
})(AuthorCustom || (AuthorCustom = {}));
console.log(AuthorCustom);
function getItems(paginatedData) {
    return paginatedData.items;
}
exports.getItems = getItems;
var apiData = { /* some paginated data */};
var items = getItems(apiData);
var items2 = getItems(apiData); // typescript is able to infer the generic type in this case
