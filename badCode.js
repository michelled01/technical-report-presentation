const response = {
  data: { id: 2, description: "A description" },
};

console.log(response.data()); // TypeError: response.data is not a function
console.log(response.desription); // undefined
console.log(response.id); // undefined
console.log(response.data.description.toLowerCase); // [Function: toLowerCase]
