import { useState, useEffect } from "react";
import { Paginated, Committee } from "./types";

function App() {
  const [data, setData] = useState(null);

  useEffect(() => {
    fetch("committees.json")
      .then((response) => response.json())
      .then((d) => setData(d));
  }, []);

  // note that output of this function is implicitly typed
  function getNumPages(data : Paginated<any>) {
    return Math.ceil(data.total_items / data.items_per_page)
  }

  if (data !== null) {
    return (
      <div>
        <pre>{JSON.stringify(data, null, 2)}</pre>
      </div>
    );
  }

  return <></>;
}

export default App;
