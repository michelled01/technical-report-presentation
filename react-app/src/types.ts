export interface Preview {
    "id": number,
    "name": string,
}

export interface Committee {
    "id": number,
    "name": string,
    "type": "Standing" | "Special" | "Select" | "Other" | "Joint" | "Commission or Caucus",
    "chamber": "House" | "Senate" | "Joint",
    "established_date": string,
    "active": boolean,
    "website_url"?: string,
    "relations": {
        "total_members": number,
        "total_bills": number,
        "chair": Preview,
        "members": Preview[],
        "bills": Preview[],
    },
}

export interface Paginated<T> {
    "page": number,
    "items_per_page": number,
    "total_items": number,
    "items_this_page": number,
    "sort_by": string,
    "sort_ascending": boolean,
    "items": T[],
}
