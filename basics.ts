// @ts-nocheck
// There are 3 base types in TypeScript:
const subjectArea: string = "CS";
const courseCode: number = 373;
const active: boolean = true;

// If initializing from literals, the compiler can infer the types,
// so type annotation is not required.
const subjectArea2 = "CS";
const courseCode2 = 373;
const active2 = true;

// typing arrays
const list1: Array<number> = [1, 2, 3];
const list2: number[] = [1, 2, 3];
const list3 = [1, 2, 3];

// If type annotations are omitted in contexts where the compiler cannot infer the types, the
// behavior depends on the compiler flag noImplicitAny. If enabled,
// the compiler will throw an error; otherwise, the compiler will implicitly assign the any type.
// The any type can also be explicitly given in a type annotation:

const response = {
  data: {
    id: 2,
    description: "A description",
    // let's assume the API returns an unknown type for the property below
    mysteryType: "Who knows what this could be?",
  },
};

const data: any = response.data; // What's the problem with this?

// tuples in TS
let tuple: [string, number, any] = ["e1", 1, {}];
tuple = ["e2", 2]; //-> Type '[string, number]' is not assignable to type '[string, number, any]'.

// tuples don't exist in JS
tuple.push(false); // will this work at runtime?

//////////////////////////////////
// Typing functions
/////////////////////////////////

function display(message: string): number {
  console.log("Hello world");
  return 42;
}

display(4); // Argument of type 'number' is not assignable to parameter of type 'string'.

// Why don't we need typing in this case?
const message = ["Hello", "world"];
message.forEach(function (str) {
  console.log(str);
});

function displayOrNumber(message: string | number) {
  // @ts-ignore
  console.log(message.toLowerCase()); // Property 'toLowerCase' does not exist on type 'number'

  if (typeof message === "string") {
    console.log(message.toLowerCase());
  }
  else {
    console.log(message + 30);
  }
}

displayOrNumber("HELLO WORLD"); // hello world
displayOrNumber(12); // 42